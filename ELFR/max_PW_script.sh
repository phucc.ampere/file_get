testtime=$1
iteration=$2

loopn=$(expr $testtime / $iteration)

for i in $(seq 1 $iteration)
do
    echo 'running loop Max_power '$i' iteration'
    ./Max_power_BLT/RUN.AMP Max_power_BLT/AMP_Mxp4 $loopn &
    sleep $loopn
done